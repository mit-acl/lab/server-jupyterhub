Per User Docker
===============

This `Dockerfile` describes the docker image that is used for each user of jupyterhub. Note that this particular `Dockerfile` has been setup for the 16.32 class to install [`ipopt`](https://coin-or.github.io/Ipopt/INSTALL.html#COINBREW) (via `coinbrew`) with [`HSL`](http://www.hsl.rl.ac.uk/ipopt/) support.

To install with `HSL` support, you must download the **[Source]** of `Coin-HSL Full (Stable)`. This requires filling out a form to get the license and waiting up to 24 hours (typically 4-8). At the time of this writing, the latest stable version is 2015.06.23. Place the file (`coinhsl-2015.06.23.tar.gz`) in this directory before building the Docker image (with `make notebook_image` in root directory of this repo).
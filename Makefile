include .env

.DEFAULT_GOAL=build

network:
	@docker network inspect $(DOCKER_NETWORK_NAME) >/dev/null 2>&1 || docker network create $(DOCKER_NETWORK_NAME)

volumes:
	@docker volume inspect $(DATA_VOLUME_HOST) >/dev/null 2>&1 || docker volume create --name $(DATA_VOLUME_HOST)
	@docker volume inspect $(DB_VOLUME_HOST) >/dev/null 2>&1 || docker volume create --name $(DB_VOLUME_HOST)

self-signed-cert:
	# make a self-signed cert

secrets/postgres.env:
	@echo "Generating postgres password in $@"
	@echo "POSTGRES_PASSWORD=$(shell openssl rand -hex 32)" > $@

secrets/oauth.env:
	@echo "Need oauth.env file in secrets with GitHub parameters"
	@exit 1

secrets/hash.env:
	@echo "Generating HashAuthenticator secret key in $@"
	@echo "HASH_AUTH_SECRET_KEY=$(shell openssl rand -hex 32)" > $@
	@echo "HASH_AUTH_LENGTH=15" >> $@

check-files: secrets/hash.env secrets/postgres.env

pull:
	docker pull $(DOCKER_NOTEBOOK_IMAGE)

notebook_image: pull singleuser/Dockerfile
	docker build -t $(LOCAL_NOTEBOOK_IMAGE) \
		--build-arg JUPYTERHUB_VERSION=$(JUPYTERHUB_VERSION) \
		--build-arg DOCKER_NOTEBOOK_IMAGE=$(DOCKER_NOTEBOOK_IMAGE) \
		singleuser

hashpass: secrets/hash.env
	$(eval HASH_AUTH_SECRET_KEY := $(shell sed -n 's/^HASH_AUTH_SECRET_KEY=\(.*\)/\1/p' < secrets/hash.env))
	$(eval HASH_AUTH_LENGTH := $(shell sed -n 's/^HASH_AUTH_LENGTH=\(.*\)/\1/p' < secrets/hash.env))
	@docker run -it --rm ${HUB_CONTAINER_NAME} /bin/bash -c 'hashauthpw -l${HASH_AUTH_LENGTH} ${USER} ${HASH_AUTH_SECRET_KEY}'

build: check-files network volumes
	docker-compose build

.PHONY: network volumes check-files pull notebook_image build hashpass
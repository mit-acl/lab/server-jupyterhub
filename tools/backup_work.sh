#!/bin/bash

BKDIR="work_backup_$(date +%Y%m%d)"

mkdir $BKDIR

# Assumption: user volumes are named `jupyterhub-user-*` (double check with `docker volume ls`)
# Assumption: docker volumes are stored in /var/lib/docker/volumes (double check with `docker volume inspect jupyterhub-user-jhow`)
docker run --rm -it \
  --mount type=bind,src=/var/lib/docker,dst=/var/lib/docker,readonly \
  --mount type=bind,src=$(pwd)/$BKDIR,dst=/data busybox \
  /bin/sh -c "cp -r /var/lib/docker/volumes/jupyterhub-user-* /data; chown -R $(id -u):$(id -g) /data"

# remove .git directories -- no repos
find . -name ".git" -exec rm -rf {} \;

# remove ipynb_checkpoints
find . -name ".ipynb_checkpoints" -exec rm -rf {} \;

tar zcvf "${BKDIR}.tgz" $BKDIR
rm -rf $BKDIR

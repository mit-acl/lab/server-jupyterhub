Tools
=====

## Backing up `work` Directory.

Make a backup of student's work. Note that this **only** backs up the `work` directory.

Run `./backup_work.sh` -- this will create a `work_backup_xxxx.tgz` tar file.

Secrets
=======

This directory contains secrets in the form of environment variables that get used in `jupyterhub_config.py` via docker-compose. Secrets are auto-generated and might look like:

## `hash.env`

```bash
HASH_AUTH_SECRET_KEY=ad2cf29154d0271591fa47aced4f1aa54a0d32d0239654eab6f82fa0e2af307c
HASH_AUTH_LENGTH=15
```

This is for the [`HashAuthenticator`](https://github.com/thedataincubator/jupyterhub-hashauthenticator), which allows the admin to create users with passwords equal to the hash of their username + secret key.

## `postgres.env`

```bash
POSTGRES_PASSWORD=7bfbc3f77b455dd7caf82a8d4c8cd6c429b8c4716f7f1b35099812b8119a1b1e
```

## `oauth.env`

This secret file is not currently being used, but may be of interest if OAuth/SSO is used for authentication in the future. In that case, please follow the instructions at [jupyterhub/jupyterhub-deploy-docker](https://github.com/jupyterhub/jupyterhub-deploy-docker#authenticator-setup) on setting up the OAuth service. Note that edits will need to be made to `jupyterhub_config.py`: The `HashAuthenticator` needs to be commented out, and the following lines commented in:

```bash
# Authenticate users with GitHub OAuth
# c.JupyterHub.authenticator_class = 'oauthenticator.GitHubOAuthenticator'
# c.GitHubOAuthenticator.oauth_callback_url = os.environ['OAUTH_CALLBACK_URL']
```

Additionally, the `oauth.env` secret file needs to be populated with something like:

```bash
GITHUB_CLIENT_ID=<secret>
GITHUB_CLIENT_SECRET=<secret>
OAUTH_CALLBACK_URL=http://example.com/hub/oauth_callback
```
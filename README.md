JupyterHub Server Setup
=======================

This repo contains the configuration, `Dockerfile`s, and `docker-compose.yml` files needed to quickly launch a JupyterHub server. Everything is Dockerized which means it is easy to set this server up in both development and production.

The Dockerized JupyterHub stuff largely follows the [jupyterhub/jupyterhub-deploy-docker](https://github.com/jupyterhub/jupyterhub-deploy-docker) example. We add the `nginx-proxy` and `docker-gen` stuff.

**NOTE:** Repeatedly remind users that they should regularly make backups!

## Getting Started

For a visualization of what is going on, see

  - [nginx-proxy/docker-letsencrypt-nginx-proxy-companion](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion) for an illustration of what the `server/docker-compose.yml` is doing (JupyterHub is e.g., `app1`)
  - [jupyterhub/jupyterhub-deploy-docker](https://github.com/jupyterhub/jupyterhub-deploy-docker#technical-overview) for an illustration of what JupyterHub docker (`docker-compose.yml`) is doing.

### Setup

1. Install the [`docker-compose-plugin`](https://docs.docker.com/compose/install/linux/) (e.g., Docker version 20.10.21 with Docker Compose version v2.12.2).
2. `cp .env.sample .env` and edit accordingly (especially `JUPYTERHUB_DOMAINS`)
3. `make build`
4. Follow hsl instructions in `singleuser` dir
5. `make notebook_image`
6. Follow instructions in `server` dir (including running `docker compose up -d` to run server)
7. `docker-compose up -d`. For whatever reason, you may need  to run this command twice (i.e., stop with `docker-compose down` and then `docker-compose up -d` again to setup the postgresql database)
8. default admin user will be `aclswarm`. Find password with `make hashpass USER=aclswarm`
9. create new users in web interface -- use `make hashpass USER=<user>` to find password.

### Upgrading, Adding Additional Software to JuyterHub

Each user causes a new docker container to be started. The image from which these containers are built from is in the `singleuser` dir. Update the software in that Dockerfile. Then, build the image using `make notebook_image`. Note that data *should* be preserved (but never take chances---regularly back up data!).

### Backing up work

 Work should be regularly backed up by the user. You can also backup all the user files (e.g., to grade files) by using the script `tools/backup_work.sh`.
 

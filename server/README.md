Server Infrastructure
=====================

1. Install Docker and docker-compose.
1. Create a network: `docker network create nginx-web`
1. `./get_nginx_tmp.sh`
2. `docker-compose build`
3. `docker-compose up -d`

1. I get an `Error starting userland proxy: ... address already in use` when trying to launch `docker-compose up`
  - Try `sudo netstat -pna | grep 443`
